### DNA: Counting DNA Nucleotides
def dna(dna):
    f = { 'A': 0, 'C': 0, 'G': 0, 'T': 0 }
    for ch in dna:
        f[ch] += 1
    return f


### RNA: Transcribing DNA into RNA
def rna(dna):
    return dna.replace('T', 'U')


### REVC: Complementing a Strand of DNA
def revc(dna):
    compl = { 'A': 'T', 'T': 'A', 'C': 'G', 'G': 'C' }
    r = ''
    for ch in dna[::-1]:
        r += compl[ch]
    return r


### FIB: Rabbits and Recurrence Relations
def fib(n, k):
    a = [1, 1]
    for i in range(2, n):
        a.append(a[-1] + k * a[-2])
    return a[n - 1]


### GC: Computing GC Content
def gc(name):
    maxGc = 0
    with open(name, 'r') as file:
        line = file.readline()
        while len(line) > 1 and line[0] == '>':
            currentId = line[1:].strip()
            currentCount = 0
            currentLen = 0
            line = file.readline()
            while line != '' and line[0] != '>':
                dna = line.strip()
                currentCount += dna.count('G') + dna.count('C')
                currentLen += len(dna)
                line = file.readline()
            if currentCount / currentLen >= maxGc:
                maxGc = currentCount / currentLen
                maxId = currentId
    return maxId, maxGc


### HAMM: Counting Point Mutations
def hamm(s, t):
    return sum([ x != y for x, y in zip(s, t) ])


### IPRB: Mendel's First Law


### PROT: Translating RNA into Protein


### SUBS: Finding a Motif in DNA
def subs(s, t):
    res = []
    i = 0
    while True:
        i = s.find(t, i) + 1
        if i != 0:
            res.append(i)
        else:
            break
    return res


### FIBD: Mortal Fibonacci Rabbits
def fibd(n, m):
    a = [1] * n
    for i in range(2, m):
        a[i] = a[i - 1] + a[i - 2]
    for i in range(m, n):
        a[i] = a[i - 1] + a[i - 2] - a[(i - m) - 1]
    return a[-1]
